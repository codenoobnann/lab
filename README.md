Docker compose
===
## What is docker compose?
* [overview](https://docs.docker.com/compose/overview/)

## Why should we use docker compose?
Docker compose can help us to create lots of container for running OurChain nodes without lots of commands.Instead we create lots of conatainer with a yml file we define in advance.

## How to create 100 containers for testing and running OurChain at one time?
### Environment


* [Install compose](https://docs.docker.com/compose/install/)

### get python script(use to define docker-compose yml) and Dockerfile
<pre><code>$git clone https://codenoobnann@bitbucket.org/codenoobnann/lab.git

$cd lab
</code></pre>
### Start up 100 node
<pre><code>$python buildcomp.py #after this commad we will get a file "docker-compose.yml"

$docker-compose up 
</code></pre>
done here
we can check the status with command below <pre><code>$docker-compose ps 
</code></pre>
### other command 
[docker-compose getting start](https://docs.docker.com/compose/gettingstarted/)

<pre><code>$docker-compose stop #to stop all container

$docker-compose ps #to see what is currently running
</code></pre>


