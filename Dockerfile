FROM ubuntu:latest
MAINTAINER b03902092@ntu.edu.tw

# main

RUN cd ~ && apt update && apt install git -y && git clone https://bitbucket.org/lab408/ourchain-release.git 
RUN apt-get install build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils libboost-all-dev software-properties-common wget net-tools -y && add-apt-repository ppa:bitcoin/bitcoin -y && apt-get update && apt-get install libdb4.8-dev libdb4.8++-dev -y


# (optional)
## miniupnpc
RUN apt-get install libminiupnpc-dev -y
## ZMQ
RUN apt-get install libzmq3-dev -y
## Qt 5
RUN apt-get install libqt5gui5 libqt5core5a libqt5dbus5 qttools5-dev qttools5-dev-tools libprotobuf-dev protobuf-compiler -y
## libqrencode 
RUN apt-get install libqrencode-dev -y
## bitcoin-cli with the --repeat
RUN wget -P ~/ourchain-release/src/ https://www.dropbox.com/s/jh7e0az5ry4owum/bitcoin-cli.cpp

# make

RUN cd ~/ourchain-release && ~/ourchain-release/autogen.sh && ~/ourchain-release/configure --disable-tests && make && make install && ldconfig && mkdir ~/.bitcoin/
